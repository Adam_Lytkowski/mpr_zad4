package main.java.pl.edu.pjwstk.main.service.repository;

import main.java.pl.edu.pjwstk.main.domain.Address;
import main.java.pl.edu.pjwstk.main.domain.ClientDetails;
import main.java.pl.edu.pjwstk.main.domain.Order;
import main.java.pl.edu.pjwstk.main.domain.OrderItem;

public interface IRepositoryCatalog {
	public IRepository<Address> getAddresses();
	public IRepository<ClientDetails> getClientDetails();
	public IRepository<OrderItem> getOrderItems();
	public IRepository<Order> getOrders();
	public void commit();
}
