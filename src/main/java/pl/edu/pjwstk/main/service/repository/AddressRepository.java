package main.java.pl.edu.pjwstk.main.service.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.pl.edu.pjwstk.main.domain.Address;
import main.java.pl.edu.pjwstk.main.domain.builder.IEntityBuilder;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWork;

public class AddressRepository extends Repository<Address> {

	public AddressRepository(Connection connection, IEntityBuilder<Address> builder, IUnitOfWork uow) {
		super(connection, builder, uow);

		createTableSQL = "CREATE TABLE address("
				+ "id BIGINT PRIMARY KEY, "
				+ "street VARCHAR(20), "
				+ "buildingnumber VARCHAR(2), "
				+ "flatnumber VARCHAR(2), "
				+ "postalcode CHAR(6), "
				+ "city VARCHAR(20), "
				+ "country VARCHAR(20))";
		
		try {
			Statement statement = connection.createStatement();
	
			ResultSet rs = connection.getMetaData().getTables(null, null, null,null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("address".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}
			
			if (!tableExists)
				statement.executeUpdate(createTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void setUpUpdateQuery(Address a) throws SQLException {
		updateStatement.setString(1, a.getStreet());
		updateStatement.setString(2, a.getBuildingNumber());
		updateStatement.setString(3, a.getFlatNumber());
		updateStatement.setString(4, a.getPostalCode());
		updateStatement.setString(5, a.getCity());
		updateStatement.setString(6, a.getCountry());
		updateStatement.setLong(7, a.getId());
	}

	@Override
	protected void setUpInsertQuery(Address a) throws SQLException {
		insertStatement.setLong(1, a.getId());
		insertStatement.setString(2, a.getStreet());
		insertStatement.setString(3, a.getBuildingNumber());
		insertStatement.setString(4, a.getFlatNumber());
		insertStatement.setString(5, a.getPostalCode());
		insertStatement.setString(6, a.getCity());
		insertStatement.setString(7, a.getCountry());
	}

	@Override
	protected String getTableName() {
		return "address";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE address SET street=?, buildingnumber=?, flatnumber=?, postalcode=?, city=?, country=? WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO address VALUES(?, ?, ?, ?, ?, ?, ?)";
	}

}
