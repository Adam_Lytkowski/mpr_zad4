package main.java.pl.edu.pjwstk.main.service.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.java.pl.edu.pjwstk.main.domain.OrderItem;
import main.java.pl.edu.pjwstk.main.domain.builder.IEntityBuilder;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWork;

public class OrderItemRepository extends Repository<OrderItem> {
	
	protected PreparedStatement readByOrderIdStatement;
	
	protected String readByOrderIdStatementSQL = "SELECT * FROM " + getTableName() + " WHERE orderid=?";

	protected OrderItemRepository(Connection connection, IEntityBuilder<OrderItem> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
		
		createTableSQL = "CREATE TABLE orderitem("
				+ "id BIGINT PRIMARY KEY, "
				+ "name VARCHAR(20),"
				+ "description VARCHAR(120), "
				+ "price DOUBLE, "
				+ "orderid BIGINT)";
	
		try {
			readByOrderIdStatement = connection.prepareStatement(readByOrderIdStatementSQL);
			
			Statement statement = connection.createStatement();
	
			ResultSet rs = connection.getMetaData().getTables(null, null, null,null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("orderitem".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}
	
			if (!tableExists)
				statement.executeUpdate(createTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void setUpUpdateQuery(OrderItem oi) throws SQLException {
		updateStatement.setString(1, oi.getName());
		updateStatement.setString(2, oi.getDescription());
		updateStatement.setDouble(3, oi.getPrice());
		updateStatement.setLong(4, oi.getOrderId());
		updateStatement.setLong(5, oi.getId());
		
	}

	@Override
	protected void setUpInsertQuery(OrderItem oi) throws SQLException {
		insertStatement.setLong(1, oi.getId());
		insertStatement.setString(2, oi.getName());
		insertStatement.setString(3, oi.getDescription());
		insertStatement.setDouble(4, oi.getPrice());
		insertStatement.setLong(5, oi.getOrderId());
	}
	
	public List<OrderItem> readByOrderId(long id) {
		List<OrderItem> orderItemList = new ArrayList<OrderItem>();
		try {
			readByIdStatement.setLong(1, id);
			ResultSet rs = readByIdStatement.executeQuery();
			while(rs.next()) {
				orderItemList.add(builder.build(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orderItemList;
	}
	

	@Override
	protected String getTableName() {
		return "orderitem";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE orderitem SET name=?, description=?, price=?, orderid=? WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO orderitem VALUES(?, ?, ?, ?, ?)";
	}

}
