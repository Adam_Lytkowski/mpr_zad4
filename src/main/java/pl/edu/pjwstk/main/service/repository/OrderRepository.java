package main.java.pl.edu.pjwstk.main.service.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import main.java.pl.edu.pjwstk.main.domain.Order;
import main.java.pl.edu.pjwstk.main.domain.OrderItem;
import main.java.pl.edu.pjwstk.main.domain.builder.IEntityBuilder;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWork;

public class OrderRepository extends Repository<Order> {
	
	private AddressRepository addressRepository;
	private ClientDetailsRepository clientDetailsRepository;
	private OrderItemRepository orderItemRepository;

	protected OrderRepository(Connection connection, IEntityBuilder<Order> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
		
		createTableSQL = "CREATE TABLE orders("
				+ "id BIGINT PRIMARY KEY, "
				+ "clientdetailsid BIGINT, "
				+ "addressid BIGINT)";
		
		try {
			Statement statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null,null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("orders".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}
			
			if (!tableExists)
				statement.executeUpdate(createTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void setUpUpdateQuery(Order o) throws SQLException {
		updateStatement.setLong(1, o.getClient().getId());
		updateStatement.setLong(2, o.getDeliveryAddress().getId());
		updateStatement.setLong(3, o.getId());
	}

	@Override
	protected void setUpInsertQuery(Order o) throws SQLException {
		insertStatement.setLong(1, o.getId());
		insertStatement.setLong(2, o.getClient().getId());
		insertStatement.setLong(3, o.getDeliveryAddress().getId());
	}
	
	@Override
	public void save(Order order)
	{
		uow.markAsNew(order, this);
		uow.markAsNew(order.getDeliveryAddress(), addressRepository);
		uow.markAsNew(order.getClient(), clientDetailsRepository);
		
		List<OrderItem> oil = order.getItems();
		Iterator<OrderItem> oilIterator = oil.iterator();
		while(oilIterator.hasNext()) {
			OrderItem oi = (OrderItem) oilIterator.next();		
			uow.markAsNew(oi, orderItemRepository);
		}
	}
	
	public void saveOnlyOrder(Order order)
	{
		uow.markAsNew(order, this);
	}

	@Override
	public void update(Order order)
	{
		uow.markAsDirty(order, this);
		uow.markAsDirty(order.getDeliveryAddress(), addressRepository);
		uow.markAsDirty(order.getClient(), clientDetailsRepository);
		
		List<OrderItem> oil = order.getItems();
		Iterator<OrderItem> oilIterator = oil.iterator();
		while(oilIterator.hasNext()) {
			OrderItem oi = (OrderItem) oilIterator.next();		
			uow.markAsDirty(oi, orderItemRepository);
		}
	}
	
	public void updateOnlyOrder(Order order)
	{
		uow.markAsDirty(order, this);
	}

	@Override
	public void delete(Order order)
	{	
		uow.markAsDeleted(order, this);
		uow.markAsDeleted(order.getDeliveryAddress(), addressRepository);
		uow.markAsDeleted(order.getClient(), clientDetailsRepository);
		
		List<OrderItem> oil = order.getItems();
		Iterator<OrderItem> oilIterator = oil.iterator();
		while(oilIterator.hasNext()) {
			OrderItem oi = (OrderItem) oilIterator.next();		
			uow.markAsDeleted(oi, orderItemRepository);
		}
	}
	
	public void deleteOnlyOrder(Order order)
	{
		uow.markAsDeleted(order, this);
	}
	
	public void setRepositories(AddressRepository addressRepository, ClientDetailsRepository clientDetailsRepository,
					OrderItemRepository orderItemRepository) {
		setAddressRepository(addressRepository);
		setClientDetailsRepository(clientDetailsRepository);
		setOrderItemRepository(orderItemRepository);
	}

	public void setAddressRepository(AddressRepository addressRepository) {
		this.addressRepository = addressRepository;
	}
	
	public void setClientDetailsRepository(ClientDetailsRepository clientDetailsRepository) {
		this.clientDetailsRepository = clientDetailsRepository;
	}
	
	public void setOrderItemRepository(OrderItemRepository orderItemRepository) {
		this.orderItemRepository = orderItemRepository;
	}
	
	@Override
	protected String getTableName() {
		return "orders";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE orders SET clientdetailsid=?, addressid=? WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO orders VALUES(?, ?, ?)";
	}

}
