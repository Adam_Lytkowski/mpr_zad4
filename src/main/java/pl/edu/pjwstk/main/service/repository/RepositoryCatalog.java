package main.java.pl.edu.pjwstk.main.service.repository;

import java.sql.Connection;

import main.java.pl.edu.pjwstk.main.domain.Address;
import main.java.pl.edu.pjwstk.main.domain.ClientDetails;
import main.java.pl.edu.pjwstk.main.domain.Order;
import main.java.pl.edu.pjwstk.main.domain.OrderItem;
import main.java.pl.edu.pjwstk.main.domain.builder.AddressBuilder;
import main.java.pl.edu.pjwstk.main.domain.builder.ClientDetailsBuilder;
import main.java.pl.edu.pjwstk.main.domain.builder.OrderBuilder;
import main.java.pl.edu.pjwstk.main.domain.builder.OrderItemBuilder;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog {

	@SuppressWarnings("unused")
	private Connection connection;
	private IUnitOfWork uow;
	
	private AddressRepository addressRepository;
	private ClientDetailsRepository clientDetailsRepository;
	private OrderItemRepository orderItemRepository;
	private OrderRepository orderRepository;
	
	private AddressBuilder addressBuilder;
	private ClientDetailsBuilder clientDetailsBuilder;
	private OrderItemBuilder orderItemBuilder;
	private OrderBuilder orderBuilder;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		this.connection = connection;
		this.uow = uow;
		
		addressBuilder = new AddressBuilder();
		clientDetailsBuilder = new ClientDetailsBuilder();
		orderItemBuilder = new OrderItemBuilder();
		
		addressRepository = new AddressRepository(connection, addressBuilder, uow);
		clientDetailsRepository = new ClientDetailsRepository(connection, clientDetailsBuilder, uow);
		orderItemRepository = new OrderItemRepository(connection, orderItemBuilder, uow);
		
		orderBuilder = new OrderBuilder();
		orderBuilder.setRepositories(addressRepository, clientDetailsRepository, orderItemRepository);
		
		orderRepository = new OrderRepository(connection, orderBuilder, uow);
		orderRepository.setRepositories(addressRepository, clientDetailsRepository, orderItemRepository);
	}
	
	public IRepository<Address> getAddresses() {
		return addressRepository;
	}

	public IRepository<ClientDetails> getClientDetails() {
		return clientDetailsRepository;
	}

	public IRepository<OrderItem> getOrderItems() {
		return orderItemRepository;
	}

	public IRepository<Order> getOrders() {
		return orderRepository;
	}

	public void commit() {
		uow.commit();
	}

}
