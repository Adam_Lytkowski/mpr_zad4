package main.java.pl.edu.pjwstk.main.service.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.pl.edu.pjwstk.main.domain.ClientDetails;
import main.java.pl.edu.pjwstk.main.domain.builder.IEntityBuilder;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWork;

public class ClientDetailsRepository extends Repository<ClientDetails> {

	protected ClientDetailsRepository(Connection connection, IEntityBuilder<ClientDetails> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
		
		createTableSQL = "CREATE TABLE clientdetails("
				+ "id BIGINT PRIMARY KEY, "
				+ "name VARCHAR(20), "
				+ "surname VARCHAR(30), "
				+ "login VARCHAR(20))";
		
		try {
			Statement statement = connection.createStatement();
	
			ResultSet rs = connection.getMetaData().getTables(null, null, null,null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("clientdetails".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}
			
			if (!tableExists)
				statement.executeUpdate(createTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void setUpUpdateQuery(ClientDetails cd) throws SQLException {
		updateStatement.setString(1, cd.getName());
		updateStatement.setString(2, cd.getSurname());
		updateStatement.setString(3, cd.getLogin());
		updateStatement.setLong(4, cd.getId());
	}

	@Override
	protected void setUpInsertQuery(ClientDetails cd) throws SQLException {
		insertStatement.setLong(1, cd.getId());
		insertStatement.setString(2, cd.getName());
		insertStatement.setString(3, cd.getSurname());
		insertStatement.setString(4, cd.getLogin());
	}

	@Override
	protected String getTableName() {
		return "clientdetails";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE clientdetails SET name=?, surname=?, login=? WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO clientdetails VALUES(?, ?, ?, ?)";
	}

	
	
}
