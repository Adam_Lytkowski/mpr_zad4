package main.java.pl.edu.pjwstk.main.service.repository;

import java.util.List;

public interface IRepository<TEntity> {
	
	public TEntity readById(long id);
	public List<TEntity> readAll();
	
	public void save(TEntity entity);
	public void update(TEntity entity);
	public void delete(TEntity entity);
		
}
