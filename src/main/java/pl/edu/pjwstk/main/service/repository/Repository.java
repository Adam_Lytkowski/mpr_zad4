package main.java.pl.edu.pjwstk.main.service.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import main.java.pl.edu.pjwstk.main.domain.builder.IEntityBuilder;
import main.java.pl.edu.pjwstk.main.domain.entity.Entity;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWork;
import main.java.pl.edu.pjwstk.main.domain.unitofwork.IUnitOfWorkRepository;

public abstract class Repository<TEntity extends Entity> implements IRepository<TEntity>, IUnitOfWorkRepository {
	
	protected Connection connection;
	protected PreparedStatement insertStatement;
	protected PreparedStatement readAllStatement;
	protected PreparedStatement readByIdStatement;
	protected PreparedStatement updateStatement;
	protected PreparedStatement deleteStatement;
	
	protected IUnitOfWork uow;
	
	protected IEntityBuilder<TEntity> builder;
	
	protected String readAllStatementSQL = "SELECT * FROM " + getTableName();
	protected String readByIdStatementSQL = "SELECT * FROM " + getTableName() + " WHERE id=?";
	protected String deleteAllStatementSQL = "DELETE FROM " + getTableName();
	protected String deleteStatementSQL = "DELETE FROM " + getTableName() + " WHERE id=?";
	protected String createTableSQL;
	
	protected final String PATH = "jdbc:hsqldb:hsql://localhost/workdb";
	
	protected Repository(Connection connection, IEntityBuilder<TEntity> builder, IUnitOfWork uow) {
		this.connection = connection;
		this.builder = builder;
		this.uow=uow;
		
		try {
			readByIdStatement = connection.prepareStatement(readByIdStatementSQL);
			readAllStatement = connection.prepareStatement(readAllStatementSQL);
			deleteStatement = connection.prepareStatement(deleteStatementSQL);
			updateStatement = connection.prepareStatement(getUpdateQuery());
			insertStatement = connection.prepareStatement(getInsertQuery());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public TEntity readById(long id) {
		try {
			readByIdStatement.setLong(1, id);
			ResultSet rs = readByIdStatement.executeQuery();
			while(rs.next()) {
				return builder.build(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private boolean isEntityInDatabase(Entity e) {
		try {
			String query = "SELECT id FROM " + getTableName() + " WHERE id=" + e.getId();
			PreparedStatement checkIfEntityExistInDatabaseStatement = connection.prepareStatement(query);
			ResultSet rs = checkIfEntityExistInDatabaseStatement.executeQuery();
			if(rs.next()) {
				if(e.getId() == rs.getInt(1)) return true;
			} else return false;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		}
		return false;
	}
	
	public List<TEntity> readAll() {
		List<TEntity> result = new ArrayList<TEntity>();
		
		try {
			ResultSet rs= readAllStatement.executeQuery();
			while(rs.next())
			{
				result.add(builder.build(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public void persistDelete(Entity entity) {
		try {
			deleteStatement.setLong(1, entity.getId());
			deleteStatement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void persistInsert(Entity entity) {
		try {
			setUpInsertQuery((TEntity)entity);
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void persistUpdate(Entity entity) {
		try {
			setUpUpdateQuery((TEntity) entity);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void save(TEntity entity)
	{
		if(entity == null) 
			throw new IllegalArgumentException("Null argument is not allowed in method save.");
		uow.markAsNew(entity, this);
	}

	public void update(TEntity entity)
	{
		if(entity == null) 
			throw new IllegalArgumentException("Null argument is not allowed in method update.");
		if(!isEntityInDatabase(entity)) throw new IllegalArgumentException("Entity passed in argument don't exist in database.");
		uow.markAsDirty(entity, this);
	}

	public void delete(TEntity entity)
	{
		if(entity == null) 
			throw new IllegalArgumentException("Null argument is not allowed in method delete.");
		if(!isEntityInDatabase(entity)) throw new IllegalArgumentException("Entity passed in argument don't exist in database.");
		uow.markAsDeleted(entity, this);
	}
	
	protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
}
