package main.java.pl.edu.pjwstk.main.domain.unitofwork;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public interface IUnitOfWorkRepository {
	public void persistInsert(Entity entity);
	public void persistUpdate(Entity entity);
	public void persistDelete(Entity entity);
}
