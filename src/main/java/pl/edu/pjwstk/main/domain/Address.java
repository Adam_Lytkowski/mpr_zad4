package main.java.pl.edu.pjwstk.main.domain;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public class Address extends Entity {
	private String street;
	private String buildingNumber;
	private String flatNumber;
	private String postalCode;
	private String city;
	private String country;
	
	public Address() {
	}

	public Address(long id, String street, String buildingNumber, String flatNumber, String postalCode, String city,
			String country) {
		this.setId(id);
		this.street = street;
		this.buildingNumber = buildingNumber;
		this.flatNumber = flatNumber;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
