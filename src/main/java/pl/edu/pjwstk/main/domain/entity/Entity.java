package main.java.pl.edu.pjwstk.main.domain.entity;

public abstract class Entity {
	private long id;
	protected EntityState state;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public EntityState getState() {
		return state;
	}
	
	public void setState(EntityState state) {
		this.state = state;
	}
	
	
}
