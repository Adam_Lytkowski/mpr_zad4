package main.java.pl.edu.pjwstk.main.domain;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public class ClientDetails extends Entity {
	private String name;
	private String surname;
	private String login;
	
	public ClientDetails() {
	}
	
	public ClientDetails(long id, String name, String surname, String login) {
		this.setId(id);
		this.name = name;
		this.surname = surname;
		this.login = login;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	
}
