package main.java.pl.edu.pjwstk.main.domain.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.pl.edu.pjwstk.main.domain.ClientDetails;

public class ClientDetailsBuilder implements IEntityBuilder<ClientDetails> {

	public ClientDetails build(ResultSet rs) throws SQLException {
		ClientDetails client = new ClientDetails();
		client.setId(rs.getLong("id"));
		client.setName(rs.getString("name"));
		client.setSurname(rs.getString("surname"));
		client.setLogin(rs.getString("login"));
		return client;
	}

}
