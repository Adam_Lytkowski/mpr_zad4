package main.java.pl.edu.pjwstk.main.domain.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public interface IEntityBuilder<TEntity extends Entity> {

	public TEntity build(ResultSet rs) throws SQLException;
	
}
