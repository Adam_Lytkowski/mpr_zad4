package main.java.pl.edu.pjwstk.main.domain.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.pl.edu.pjwstk.main.domain.Address;

public class AddressBuilder implements IEntityBuilder<Address> {

	public Address build(ResultSet rs) throws SQLException {
		Address a = new Address();
		a.setId(rs.getLong("id"));
		a.setStreet(rs.getString("street"));
		a.setBuildingNumber(rs.getString("buildingnumber"));
		a.setFlatNumber(rs.getString("flatnumber"));
		a.setPostalCode(rs.getString("postalcode"));
		a.setCity(rs.getString("city"));
		a.setCountry(rs.getString("country"));
		return a;
	}

}
