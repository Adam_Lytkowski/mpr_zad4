package main.java.pl.edu.pjwstk.main.domain;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public class OrderItem extends Entity {
	private String name;
	private String description;
	private double price;
	private long orderId;
	
	public OrderItem() {
	}

	public OrderItem(long id, String name, String description, double price, long orderId) {
		this.setId(id);
		this.name = name;
		this.description = description;
		this.price = price;
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	
}
