package main.java.pl.edu.pjwstk.main.domain.entity;

public enum EntityState {
	New, Dirty, Deleted
}
