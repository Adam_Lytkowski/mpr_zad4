package main.java.pl.edu.pjwstk.main.domain.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.pl.edu.pjwstk.main.domain.OrderItem;

public class OrderItemBuilder implements IEntityBuilder<OrderItem> {

	public OrderItem build(ResultSet rs) throws SQLException {
		OrderItem oi = new OrderItem();
		oi.setId(rs.getLong("id"));
		oi.setName(rs.getString("name"));
		oi.setDescription(rs.getString("description"));
		oi.setPrice(rs.getDouble("price"));
		oi.setOrderId(rs.getLong("orderid"));
		return oi;
	}

}
