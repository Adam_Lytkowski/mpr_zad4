package main.java.pl.edu.pjwstk.main.domain.builder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import main.java.pl.edu.pjwstk.main.domain.Address;
import main.java.pl.edu.pjwstk.main.domain.ClientDetails;
import main.java.pl.edu.pjwstk.main.domain.Order;
import main.java.pl.edu.pjwstk.main.domain.OrderItem;
import main.java.pl.edu.pjwstk.main.service.repository.AddressRepository;
import main.java.pl.edu.pjwstk.main.service.repository.ClientDetailsRepository;
import main.java.pl.edu.pjwstk.main.service.repository.OrderItemRepository;

public class OrderBuilder implements IEntityBuilder<Order> {	
	private AddressRepository addressRepository;
	private ClientDetailsRepository clientDetailsRepository;
	private OrderItemRepository orderItemRepository;
	
	public Order build(ResultSet rs) throws SQLException {	
		Order o = new Order();

		while (rs.next()) {
			o.setId(rs.getLong("id"));
			
			ClientDetails c = clientDetailsRepository.readById(rs.getLong("clientdetailsid"));
			o.setClient(c);
			
			Address a = addressRepository.readById(rs.getLong("addressid"));
			o.setDeliveryAddress(a);
			
			List<OrderItem> oi = orderItemRepository.readByOrderId(rs.getLong("id"));
			o.setItems(oi);
		}
		return o;
	}
	
	public void setRepositories (AddressRepository addressRepository, ClientDetailsRepository clientDetailsRepository,
		OrderItemRepository orderItemRepository) {
		setAddressRepository(addressRepository);
		setClientDetailsRepository(clientDetailsRepository);
		setOrderItemRepository(orderItemRepository);
	}
	
	public void setAddressRepository(AddressRepository addressRepository) {
		this.addressRepository = addressRepository;
	}
	
	public void setClientDetailsRepository(ClientDetailsRepository clientDetailsRepository) {
		this.clientDetailsRepository = clientDetailsRepository;
	}
	
	public void setOrderItemRepository(OrderItemRepository orderItemRepository) {
		this.orderItemRepository = orderItemRepository;
	}
}
