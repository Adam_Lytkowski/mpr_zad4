package main.java.pl.edu.pjwstk.main.domain.unitofwork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;
import main.java.pl.edu.pjwstk.main.domain.entity.EntityState;

public class UnitOfWork implements IUnitOfWork {
	
	private Connection connection;
	private Map<Entity, IUnitOfWorkRepository> entities = new LinkedHashMap<Entity, IUnitOfWorkRepository>();
	
	public UnitOfWork(Connection connection) {
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void commit() {
		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Dirty:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistInsert(entity);
				break;
			default:
				break;
			}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void rollback() {
		entities.clear();
		
	}

	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.New);
		entities.put(entity, repository);
		
	}

	public void markAsDirty(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Dirty);
		entities.put(entity, repository);
	}

	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
	}

}
