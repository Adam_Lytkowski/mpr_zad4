package main.java.pl.edu.pjwstk.main.domain;

import java.util.List;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public class Order extends Entity {
	private ClientDetails client;
	private Address deliveryAddress;
	private List<OrderItem> items;
	
	public Order() {}
	
	
	public Order(long id, Address deliveryAddress, ClientDetails client, List<OrderItem> items) {
		this.setId(id);
		this.client = client;
		this.deliveryAddress = deliveryAddress;
		this.items = items;
	}

	public ClientDetails getClient() {
		return client;
	}


	public void setClient(ClientDetails client) {
		this.client = client;
	}


	public Address getDeliveryAddress() {
		return deliveryAddress;
	}


	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}


	public List<OrderItem> getItems() {
		return items;
	}


	public void setItems(List<OrderItem> items) {
		this.items = items;
	}

	
	
}
