package main.java.pl.edu.pjwstk.main.domain.unitofwork;

import main.java.pl.edu.pjwstk.main.domain.entity.Entity;

public interface IUnitOfWork {
	public void commit();
	public void rollback();
	
	public void markAsNew(Entity entity, IUnitOfWorkRepository repository);
	public void markAsDirty(Entity entity, IUnitOfWorkRepository repository);
	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);
}
